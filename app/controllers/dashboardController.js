'use strict';

angular.module('em.home')
    .controller('DashboardController', ['$rootScope', '$scope', '$stateParams',
        function ($rootScope, $scope, $stateParams) {
            $scope.goToOverview = function () {
                doThis("overview");
            };

            $scope.goToProfile = function () {
                doThis("profile");
            };

            $scope.goToAccounts = function () {
                doThis("accounts");
            };

            $scope.goToCategory = function () {
                doThis("category");
            };

            $scope.goToPayers = function () {
                doThis("payers");
            };

            $scope.goToPayees = function () {
                doThis("payees");
            };

            $scope.goToPaymentMethod = function () {
                doThis("paymentMethod");
            };

            $scope.goToIncome = function () {
                doThis("income");
            };

            var resetActions = function () {
                $scope.showOverview = false;
                $scope.showProfile = false;
                $scope.showAccounts = false;
                $scope.showCategory = false;
                $scope.showPayers = false;
                $scope.showPayees = false;
                $scope.showPaymentMethod = false;
                $scope.showIncome = false;
            };

            var doThis = function (action) {
                resetActions();

                switch (action) {
                    case "overview":
                        $scope.showOverview = true;
                        break;
                    case  "profile":
                        $scope.showProfile = true;
                        break;
                    case "accounts":
                        $scope.showAccounts = true;
                        break;
                    case "category":
                        $scope.showCategory = true;
                        break;
                    case "payers":
                        $scope.showPayers = true;
                        break;
                    case "payees":
                        $scope.showPayees = true;
                        break;
                    case "paymentMethod":
                        $scope.showPaymentMethod = true;
                        break;
                    case "income":
                        $scope.showIncome = true;
                        break;
                    default :
                        $scope.showOverview = true;
                }
            };

            var init = function () {
                doThis($stateParams.action);
            };

            init();
        }
    ]);

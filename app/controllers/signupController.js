'use strict';

angular.module('em.home')
    .controller('SignUpController', ['$rootScope', '$scope', '$location', '$timeout', 'userService', 'spinner',
        function ($rootScope, $scope, $location, $timeout, userService, spinner) {
            $scope.user = {
                "firstName": undefined,
                "lastName": undefined,
                "birthDate": undefined,
                "gender": undefined,
                "email": undefined,
                "username": undefined,
                "password": undefined
            };

            var redirectToLoginPage = function () {
                $location.path("/login");
            };

            $scope.signUp = function () {

                spinner.forPromise(userService.signUp($scope.user).success(function () {
                    setStatus("Signed up Successfully.");
                    $timeout(function () {
                        redirectToLoginPage();
                    }, 2000);
                }).error(function (signUpResponse) {
                    setError(signUpResponse);
                }));
            };

            var setStatus = function (message) {
                $scope.status = message;
            };

            var setError = function (message) {
                $scope.error = message;
            };

            $scope.resetStatus = function () {
                $scope.status = undefined;
                $scope.error = undefined;
            };
        }]);

'use strict';

angular.module('em.home')
    .controller('LoginController', ['$rootScope', '$scope', '$location', 'sessionIdService', 'spinner', 'userService',
        function ($rootScope, $scope, $location, SessionIdService, spinner, userService) {
            $scope.user = {
                username: undefined,
                password: undefined
            };
            $scope.error = undefined;

            $scope.loginUser = function () {
                spinner.forPromise(userService.login($scope.user).success(function (loginResponse) {
                    $rootScope.SessionId = loginResponse;
                    SessionIdService.setSessionId(loginResponse);
                    SessionIdService.setCurrentUsername($scope.user.username);
                    $location.path("/user/" + $scope.user.username + "/dashboard");
                }).error(function (loginResponse) {
                    $scope.error = loginResponse;
                }));
            };

            $scope.resetError = function () {
                $scope.error = undefined;
            };

            $scope.redirectToSignupPage = function () {
                $location.path("/signup");
            };
        }]);

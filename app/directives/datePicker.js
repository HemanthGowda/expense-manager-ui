angular.module('em.home')
    .directive('datepicker', function () {
        var link = function ($scope, element, attrs, ngModel) {
            $(function(){
                var today = new Date();
                element.datepicker({
                    changeYear: true,
                    changeMonth: true,
                    maxDate: today,
                    dateFormat: 'dd-M-yy',
                    onSelect: function (dateText) {
                        $scope.$apply(function (scope) {
                            ngModel.$setViewValue(dateText);
                        });
                    }
                });
            })
        };
        return {
            restrict: 'A',
            require: 'ngModel',
            scope:{},
            link: link
        }
    });
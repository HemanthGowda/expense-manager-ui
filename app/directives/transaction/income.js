'use strict';

angular.module('em.home')
    .directive('income', ['paymentMethodService', 'payersService', 'categoryService',
        function (paymentMethodService, payersService, categoryService) {
            var link = function ($scope) {
                $scope.transaction = {
                    category: []
                };

                $scope.getCategories = function (selection) {
                    return categoryService.search(selection);
                };

                $scope.getDataResults = function (data) {
                    return data.map(function (category) {
                        return category.name;
                    });
                };

                $scope.addCategory = function (subCategory) {
                    $scope.transaction.category.push(subCategory.value);
                    $scope.$apply();
                };

                var getAllPaymentMethods = function () {
                    paymentMethodService.getAll().success(function (response) {
                        $scope.paymentMethods = response;
                    })
                };

                var getAllPayers = function () {
                    payersService.getAll().success(function (response) {
                        $scope.payers = response;
                    })
                };

                var getAllRootCategories = function () {
                    categoryService.getAllRootCategories().success(function (response) {
                        console.log(response);
                        $scope.allRootCategories = response;
                    })
                };

                getAllRootCategories();
                getAllPaymentMethods();
                getAllPayers();
            };

            return {
                link: link,
                scope: {},
                template: '<ng-include src="\'views/transaction/income.html\'" />'
            }
        }
    ]);

'use strict';

angular.module('em.home')
    .directive('subHeader', ['$location', '$state', '$rootScope', function ($location, $state, $rootScope) {
        var link = function ($scope) {
            $scope.goToAdministrationPage = function () {
                $location.path("/user/" + $rootScope.username + "/administration/overview");
            }
        };

        return {
            link: link,
            scope: {},
            template: '<ng-include src="\'views/subHeader.html\'" />'
        }
    }]);

'use strict';

angular.module('em.home')
    .directive('accounts', ['$location', '$rootScope', '$timeout', '$filter', 'spinner', 'accountService', 'currencyService',
        function ($location, $rootScope, $timeout, $filter, spinner, accountService, currencyService) {
            var link = function ($scope) {
                $scope.newAccount = {
                    name: undefined,
                    description: undefined,
                    initialAmount: undefined,
                    currencyUuid: undefined,
                    username: $rootScope.username
                };

                $scope.createAccount = function () {
                    spinner.forPromise(accountService.createAccount($scope.newAccount).success(function (loginResponse) {
                        $scope.status = "Account created successfully.";
                        getAllAccounts();
                    }).error(function (loginResponse) {
                        $scope.error = loginResponse;
                    }));
                };

                $scope.resetStatus = function () {
                    $scope.error = undefined;
                    $scope.status = undefined;
                };

                var getAllCurrencies = function () {
                    return currencyService.getAllCurrencies().success(function (response) {
                        $scope.currencies = response;
                    }).error(function (response) {
                        $scope.error = response[0];
                    });
                };

                var getAllAccounts = function(){
                    return accountService.getAccountsByUsername($rootScope.username).success(function(response){
                        $scope.allAccounts = response;
                    }).error(function(response){
                        $scope.error = response[0];
                    });
                };

                var init = function () {
                    spinner.forPromise(getAllCurrencies());
                    spinner.forPromise(getAllAccounts());
                };

                init();
            };

            return {
                link: link,
                scope: {},
                template: '<ng-include src="\'views/admin/account.html\'" />'
            }
        }
    ]);

'use strict';

angular.module('em.home')
    .directive('profile', ['$location', '$rootScope', '$timeout', '$filter', 'spinner', 'userService', function ($location, $rootScope, $timeout, $filter, spinner, userService) {
        var link = function ($scope) {
            $scope.edit = function () {
                $scope.editing = true;
            };

            $scope.cancelEdit = function () {
                init();
            };

            $scope.saveUserInfo = function () {
                spinner.forPromise(userService.updateUser($scope.user).success(function (data) {
                    setStatus("Updated Successfully.");
                    $timeout(function () {
                        init();
                    }, 1000);
                }).error(function (errorMessage) {
                    setError(errorMessage);
                }));
            };

            var getUser = function () {
                return userService.getUserByUsername($rootScope.username).success(function (data) {
                    $scope.user = new Expense.Manager.User(data);
                    $scope.user.birthDate = $filter("date")($scope.user.birthDate, 'dd-MMM-yyyy');
                });
            };

            var setStatus = function (message) {
                $scope.status = message;
            };

            var setError = function (message) {
                $scope.error = message;
            };

            $scope.resetStatus = function () {
                $scope.status = undefined;
                $scope.error = undefined;
            };

            var init = function () {
                spinner.forPromise(getUser());
                $scope.editing = false;
                $scope.resetStatus();
            };

            init();
        };

        return {
            link: link,
            scope: {},
            template: '<ng-include src="\'views/admin/profile.html\'" />'
        }
    }]);

'use strict';

angular.module('em.home')
    .directive('overview', ['$location', '$rootScope', '$filter', 'spinner', 'userService', 'accountService', function ($location, $rootScope, $filter, spinner, userService, accountService) {
        var link = function ($scope) {
            var getUser = function () {
                return userService.getUserByUsername($rootScope.username).success(function (data) {
                    $scope.user = new Expense.Manager.User(data);
                    $scope.user.birthDate = $filter("date")($scope.user.birthDate, 'dd-MMM-yyyy');
                });
            };

            var getAccounts = function () {
                return accountService.getAccountsByUsername($rootScope.username).success(function (data) {
                    $scope.accounts = data
                });
            };

            var init = function () {
                spinner.forPromise(getUser().then(getAccounts));
            };

            init();

        };

        return {
            link: link,
            scope: {},
            template: '<ng-include src="\'views/admin/overview.html\'" />'
        }
    }]);

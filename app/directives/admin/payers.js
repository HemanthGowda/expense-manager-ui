'use strict';

angular.module('em.home')
    .directive('payers', ['$location', '$rootScope', '$timeout', '$filter', 'spinner', 'payersService',
        function ($location, $rootScope, $timeout, $filter, spinner, payersService) {
            var link = function ($scope) {
                $scope.allPayers = [];

                $scope.addNew = function () {
                    spinner.forPromise(payersService.add($scope.newPayer).success(function (data) {
                        init();
                    }).error(function (error) {
                        $scope.error = error;
                    }));
                };

                $scope.toggle = function (payer) {
                    payer.isBeingEdited = !payer.isBeingEdited;
                };

                $scope.clearStatus = function(){
                    $scope.error = undefined;
                };

                var getAllPayers = function () {
                    return payersService.getAll().success(function (data) {
                        $scope.allPayers = data;
                    });
                };

                var constructNewPayer = function () {
                    $scope.newPayer = {
                        name: undefined
                    };
                };

                var init = function () {
                    constructNewPayer();
                    spinner.forPromise(getAllPayers());
                };

                init();
            };

            return {
                link: link,
                scope: {},
                template: '<ng-include src="\'views/admin/payers.html\'" />'
            }
        }
    ]);

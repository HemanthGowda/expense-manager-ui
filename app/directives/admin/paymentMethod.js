'use strict';

angular.module('em.home')
    .directive('paymentMethod', ['$location', '$rootScope', '$timeout', '$filter', 'spinner', 'paymentMethodService',
        function ($location, $rootScope, $timeout, $filter, spinner, paymentMethodService) {
            var link = function ($scope) {
                $scope.allPaymentMethods = [];

                $scope.addNew = function () {
                    spinner.forPromise(paymentMethodService.add($scope.newPaymentMethod).success(function (data) {
                        init();
                    }).error(function (error) {
                        $scope.error = error;
                    }));
                };

                $scope.clearStatus = function () {
                    $scope.error = undefined;
                };

                $scope.toggle = function (paymentMethod) {
                    paymentMethod.isBeingEdited = !paymentMethod.isBeingEdited;
                };

                var getAllPaymentMethods = function () {
                    return paymentMethodService.getAll().success(function (data) {
                        $scope.allPaymentMethods = data;
                    });
                };

                var constructNewPaymentMethod = function () {
                    $scope.newPaymentMethod = {
                        name: undefined
                    };
                };

                var init = function () {
                    constructNewPaymentMethod();
                    spinner.forPromise(getAllPaymentMethods());
                };

                init();
            };

            return {
                link: link,
                scope: {},
                template: '<ng-include src="\'views/admin/paymentMethod.html\'" />'
            }
        }
    ]);

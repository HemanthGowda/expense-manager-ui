'use strict';

angular.module('em.home')
    .directive('payees', ['$location', '$rootScope', '$timeout', '$filter', 'spinner', 'payeesService',
        function ($location, $rootScope, $timeout, $filter, spinner, payeesService) {
            var link = function ($scope) {
                $scope.allPayees = [];

                $scope.addNew = function () {
                    spinner.forPromise(payeesService.add($scope.newPayee).success(function (data) {
                        init();
                    }).error(function (error) {
                        $scope.error = error;
                    }));
                };

                $scope.toggle = function (payee) {
                    payee.isBeingEdited = !payee.isBeingEdited;
                };

                $scope.clearStatus = function(){
                    $scope.error = undefined;
                };

                var getAllPayees = function () {
                    return payeesService.getAll().success(function (data) {
                        $scope.allPayees = data;
                    });
                };

                var constructNewPayee = function () {
                    $scope.newPayee = {
                        name: undefined
                    };
                };

                var init = function () {
                    constructNewPayee();
                    spinner.forPromise(getAllPayees());
                };

                init();
            };

            return {
                link: link,
                scope: {},
                template: '<ng-include src="\'views/admin/payees.html\'" />'
            }
        }
    ]);

'use strict';

angular.module('em.home')
    .directive('category', ['$location', '$rootScope', 'categoryService', 'spinner',
        function ($location, $rootScope, categoryService, spinner) {
            var link = function ($scope) {

                $scope.categories = [];
                $scope.selectedCategory = undefined;

                var initializeNewCategory = function(){
                    $scope.newCategory = {
                        name: undefined,
                        description: undefined,
                        isSet: false,
                        subCategories: []
                    };
                };
                initializeNewCategory();

                $scope.createCategory = function () {
                    spinner.forPromise(categoryService.createCategory($scope.newCategory).success(function (categoryResponse) {
                        $scope.status = "Category created successfully.";
                        initializeNewCategory()
                    }).error(function (loginResponse) {
                        $scope.error = loginResponse;
                    }));
                };

                $scope.getCategories = function (selection) {
                    return categoryService.search(selection);
                };

                $scope.getDataResults = function (data) {
                    return data.map(function (category) {
                        return category.name;
                    });
                };

                $scope.addSubCategory = function (subCategory) {
                    $scope.newCategory.subCategories.push(subCategory.value);
                    $scope.$apply();
                };

                $scope.removeSubCategory = function (subCategory) {
                    $scope.newCategory.subCategories = _.without($scope.newCategory.subCategories, subCategory);
                };

                $scope.resetStatus = function () {
                    $scope.error = undefined;
                    $scope.status = undefined;
                };

                $scope.searchCategory = function (searchItem) {
                    $scope.selectedCategory = undefined;
                    if (!searchItem) {
                        $scope.categories = [];
                        return;
                    }
                    $scope.getCategories(searchItem).success(function (response) {
                        $scope.categories = response;
                    });
                };

                $scope.getCategories = function (selection) {
                    return categoryService.search(selection);
                };

                $scope.showCategory = function(category){
                    $scope.categories = [];
                    $scope.selectedCategory = category;
                };
            };

            return {
                link: link,
                scope: {},
                template: '<ng-include src="\'views/admin/category.html\'" />'
            }
        }]);

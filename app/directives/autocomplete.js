'use strict';

angular.module('em.home')
    .directive('categoryAutocomplete', function () {
        var link = function (scope, element) {
            var source = scope.source();
            var responseMap = scope.responseMap();
            var onSelect = scope.onSelect();
            var minLength = scope.minLength || 1;
            var clearAfter = scope.clearAfter || false;

            element.autocomplete({
                autofocus: true,
                minLength: minLength,
                source: function (request, response) {
                    source(request.term).then(function (data) {
                        var results = responseMap ? responseMap(data.data) : data.data;
                        response(results);
                    });
                },
                select: function (event, ui) {
                    if (onSelect != null) {
                        onSelect(ui.item);
                    }
                    if (clearAfter) {
                        ui.item.value = undefined;
                        ui.item.label = undefined;
                    }
                },
                search: function (event, ui) {
                    if (scope.onEdit != null) {
                        scope.onEdit(ui.item);
                    }
                    var searchTerm = $.trim(element.val());
                    if (searchTerm.length < minLength) {
                        event.preventDefault();
                    }
                }
            });
        };
        return {
            link: link,
            scope: {
                source: '&',
                responseMap: '&',
                onSelect: '&',
                onEdit: '&?',
                clearAfter: '=',
                minLength: '='
            }
        }
    });
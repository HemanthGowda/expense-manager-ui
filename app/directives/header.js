'use strict';

angular.module('em.home')
    .directive('header', ['$location', '$rootScope', 'sessionIdService', function ($location, $rootScope, sessionIdService) {
        var link = function ($scope) {
            $scope.username = $rootScope.username;
            $scope.isLoginPage = function () {
                return $location.path() == "/login";
            };

            $scope.isSignupPage = function () {
                return $location.path() == "/signup";
            };

            $scope.goToSignUpPage = function () {
                $location.url("/signup");
            };

            $scope.goToLoginPage = function () {
                $location.url("/login");
            };

            $scope.goToCreateAccountPage = function () {
                $location.url("/account");
            };

            $scope.logout = function () {
                sessionIdService.closeSession();
                $location.url("/login");
            };

            $scope.goToDashboard = function () {
                $location.url("/user/" + $rootScope.username + "/dashboard");
            }
        };

        return {
            link: link,
            scope: {},
            template: '<ng-include src="\'views/header.html\'" />'
        }
    }]);

'use strict';

angular.module('em.home', ['ngRoute', 'ngCookies', 'ui.router'])
    .config(function ($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise("/login");

        $stateProvider.state('login', {
            url: '/login',
            views:{
                content: {
                    templateUrl: 'views/login.html',
                    controller : 'LoginController'
                }
            }
        }).state("signup", {
            url: '/signup',
            views:{
                content: {
                    templateUrl: 'views/signup.html',
                    controller: 'SignUpController'
                }
            }
        }).state("user", {
            url: '/user/:username',
            abstract: true,
            views:{
                content: {
                    template: '<div ui-view="content"></div>'
                }
            },
            resolve: {
                userInitialization: function(userInitialization, $stateParams) {
                    return userInitialization($stateParams.username);
                }}
        }).state("user.dashboard", {
            url: '/dashboard',
            views:{
                content:{
                    templateUrl: 'views/dashboard.html',
                    controller: 'DashboardController'
                }
            }
        });
//        $httpProvider.defaults.headers.common['Disable-WWW-Authenticate'] = true;
    }).run(function ($rootScope, $templateCache, $location, $state, sessionIdService) {
        //Disable caching view template partials
        $rootScope.$on('$viewContentLoaded', function () {
                $templateCache.removeAll();
            }
        );

        $rootScope.$on("$stateChangeStart", function (event, toState, toParams, fromState) {
            $rootScope.username = sessionIdService.getCurrentUserName();
            if (!sessionIdService.hasSessionId() || !$rootScope.username) {
                if (toState.url == "/login" || toState.url == "/signup") {

                } else {
                    $state.go('login');
                }
            }
        })
    });
'use strict';

angular.module('em.home')
    .factory('paymentMethodService', ['$http', function ($http) {
        return {
            getAll: function () {
                return $http.get('/em/ws/rest/payment/method/getAll');
            },

            add: function (payer) {
                return $http.post('/em/ws/rest/payment/method/add', payer);
            }

        }
    }]);
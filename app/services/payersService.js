'use strict';

angular.module('em.home')
    .factory('payersService', ['$http', function ($http) {
        return {
            getAll: function () {
                return $http.get('/em/ws/rest/payer/getAll');
            },

            add: function (payer) {
                return $http.post('/em/ws/rest/payer/add', payer);
            }

        }
    }]);
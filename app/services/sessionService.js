'use strict';

angular.module('em.home')
    .factory('sessionIdService', function() {
        var sessionID = undefined;
        var currentUser= undefined;
        return {
            getSessionId: function() {
                if(sessionID=='' || sessionID==null) {
                    sessionID = localStorage.getItem("SessionId");
                }
                return sessionID;
            },

            setSessionId: function(sessId) {
                localStorage.setItem("SessionId", sessId);
                sessionID = sessId;
            },

            hasSessionId: function(){
                sessionID = localStorage.getItem("SessionId");
                return sessionID != '' && sessionID != null && sessionID != undefined;
            },

            setCurrentUsername: function(username){
                localStorage.setItem("currentUser", username);
                currentUser = username;
            },

            getCurrentUserName: function(){
                if(!currentUser){
                    currentUser = localStorage.getItem("currentUser");
                }
                return currentUser;
            },

            closeSession: function(){
                localStorage.clear();
                sessionID = undefined;
                currentUser= undefined;
            }
        }
    });
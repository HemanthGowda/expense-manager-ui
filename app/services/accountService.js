'use strict';

angular.module('em.home')
    .factory('accountService', ['$http', function($http) {
        return {
            getAccountsByUsername: function(username){
                return $http.get('/em/ws/rest/user/getAccounts', {
                    params:{
                        username: username
                    }
                });
            },
            createAccount: function(account){
                return $http.post('/em/ws/rest/account/create', account)
            }
        }
    }]);
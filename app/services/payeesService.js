'use strict';

angular.module('em.home')
    .factory('payeesService', ['$http', function ($http) {
        return {
            getAll: function () {
                return $http.get('/em/ws/rest/payee/getAll');
            },

            add: function (payer) {
                return $http.post('/em/ws/rest/payee/add', payer);
            }

        }
    }]);
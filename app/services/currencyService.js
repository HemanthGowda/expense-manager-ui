'use strict';

angular.module('em.home')
    .factory('currencyService', ['$http', function($http) {
        return {
            getAllCurrencies: function(){
                return $http.get('/em/ws/rest/currencies/getAll');
            }
        }
    }]);
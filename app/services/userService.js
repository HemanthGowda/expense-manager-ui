'use strict';

angular.module('em.home')
    .factory('userService', ['$http', function ($http) {
        return {
            getUserByUsername: function (username) {
                return $http.get('/em/ws/rest/user/getUser', {
                    params: {
                        username: username
                    }
                });
            },

            login: function (user) {
                return $http.post('/em/ws/rest/login/authenticate', user);
            },

            signUp: function (user) {
                return $http.post('/em/ws/rest/sign/up', user)
            },

            updateUser: function (user) {
                return $http.post('/em/ws/rest/user/update', user)
            }
        }
    }]);
'use strict';

angular.module('em.home')
    .factory('categoryService', ['$http', function ($http) {
        return {
            createCategory: function (category) {
                return $http.post('/em/ws/rest/category/create', category);
            },

            search: function (searchParam) {
                return $http.get('/em/ws/rest/category/search', {
                    params: {
                        searchParam: searchParam
                    }
                });
            },

            getAllRootCategories: function () {
                return $http.get('/em/ws/rest/category/allRootCategories');
            }
        }
    }]);

'use strict';

angular.module('em.home').factory('userInitialization',
    ['$rootScope', 'spinner', 'userService',
        function ($rootScope, spinner, userService) {
            return function (username) {
                $rootScope.username = username;
            }
        }]);
